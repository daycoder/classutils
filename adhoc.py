# encoding: utf-8

import logging_helper
from classutils import profiling

logging = logging_helper.setup_logging()


# =============== Test Profiling decorator ===============
@profiling.profile  # no args
def tst_fn_all():
    logging.info(u'test function all')


@profiling.profile(profile_id=u'tst_me')  # set a profile id
def tst_fn():
    logging.info(u'test function')


@profiling.profile(profile_id=u'tst_me_3')  # set a different profile id
def nested_fn():
    logging.info(u'nested function')


@profiling.profile(profile_id=u'tst_me_2')  # set a different profile id
def tst_fn_2():
    nested_fn()
    nested_fn()
    logging.info(u'test function 2')


# Enable custom profile ids
profiling.enable(u'tst_me_2')
profiling.enable(u'tst_me_3')

# Test the functions
logging.info(u'========== No Profiles (globally disabled) ==========')
tst_fn_all()  # This function should not log a profile!
tst_fn()  # This function should not log a profile!
tst_fn_2()  # This function should not log a profile!
logging.info(u'========================= ^ =========================')

logging.info(u'============ Profiles (globally enabled) ============')
# Enable profiling
profiling.enable()

tst_fn_all()
tst_fn()  # This function should not log a profile!
tst_fn_2()
logging.info(u'========================= ^ =========================')
# TODO: Move to unittests!
# ========================================================
